# Duelo de insultos

Éste proyecto se basa en el juego **Monkey Island**, concretamente en el duelo de 
insultos con rima en el que el enemigo empieza insultando y al jugador le aparecen unos
botones en el menú de acciones con los que éste puede responder al insulto.

Si el jugador escoje la respuesta que mejor responde al insulto consigue un punto,
el enemigo perdería un punto (en caso de tener alguno) y vuelve a ser turno del jugador, 
pero en éste caso le toca insultar y al enemigo responder. De lo contrário, sigue siendo
el turno del enemigo y la ganancia/pérdida de puntos se invierte en el marcador.

Si el enemigo no responde correctamente el jugador también ganaría un punto, de lo
contrário el jugador pierde uno (siendo el mínimo 0) y el enemigo lo ganaría.

**El primero que consiga 3 puntos gana** y se muestra su nombre en la pantalla final
del juego, que lleva al jugador de vuelta al menú principal.
