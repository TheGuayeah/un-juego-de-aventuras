﻿using UnityEngine;

public class Battle
{
    private int id;
    private string insult;
    private string response;

    public int Id { get => id; set => id = value; }
    public string Insult { get => insult; set => insult = value; }
    public string Response { get => response; set => response = value; }
}
