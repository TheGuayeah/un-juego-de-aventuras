﻿using System.Collections.Generic;
using UnityEngine;

public class BattleFiller
{    
    public List<Battle> allBattles = new List<Battle>()
    {
        new Battle()
        {
            Id = 0,
            Insult = "Los enemigos contra los que me enfrenté fueron aniquilados.", 
            Response ="Con tu aliento, seguro que todos fueron asfixiados."
        },
        new Battle()
        {
            Id = 1,
            Insult = "Eres tan repulsivo como una mona marrana.",
            Response ="¿Es que TANTO me parezco a tu hermana?"
        },
        new Battle()
        {
            Id = 2,
            Insult = "¡Que el cielo conserve mi vista!¡Pareces muerto como el pescado!",
            Response ="La única forma en la que te conservarás será disecado."
        },
        new Battle()
        {
            Id = 3,
            Insult = "¡Te perseguiré dia y noche sin ningún respeto!",
            Response ="Entonces se un buen perro ¡Siéntate! ¡Quieto!"
        },
        new Battle()
        {
            Id = 4,
            Insult = "¡Voy a ensartarte como a una puerca guarrería!",
            Response ="Cuando acabe CONTIGO, serás un bistec con disentería."
        },
    };

    public Battle CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<Battle>(jsonString);
    }
}
