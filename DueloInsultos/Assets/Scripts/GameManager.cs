﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    #region GAME SCENE
    public void WinnerScene()
    {
        SceneManager.LoadScene("End");
    }
    #endregion

    #region END SCENE
    public void ReturnToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
    #endregion

    #region MENU SCENE
    public void PlayGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void QuitGame()
    {
        Application.Quit();
        #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
        #endif
    }
    #endregion
}
