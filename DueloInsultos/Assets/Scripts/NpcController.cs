﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcController : MonoBehaviour
{
    private List<string> allInsults = new List<string>();

    public List<string> AllInsults { get => allInsults; set => allInsults = value; }
}
