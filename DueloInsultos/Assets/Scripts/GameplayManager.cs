﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    public enum NpcActions { INSULT, RESPOND }
    public NpcActions cpuAction;
    public enum Fighters { NPC, PLAYER }
    public Fighters turn;

    private string currentInsult;
    private string currentResponse;
    public int playerScore = 0;
    public int npcScore = 0;

    #region PRIVATE REFERENCES
    private Text battleText;
    private Text playerScoreText;
    private Text npcScoreText;
    private Transform grid;
    private GameObject btnPrefab;
    private NpcController npcController;
    private BattleFiller battleFiller;
    private Text winner;
    private Scrollbar storyScroll;
    private Scrollbar actionsScroll;
    private AudioSource clickSfx;
    #endregion

    private void Awake()
    {
        if (!IsMenuScene())
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }

    private void Start()
    {        
        #region REFERENCES
        battleFiller = new BattleFiller();
        npcController = GetComponent<NpcController>();        
        #endregion

        FillNpcInsultList();
    }

    private bool IsGameScene()
    {
        return SceneManager.GetActiveScene().name == "Game";
    }

    private bool IsEndScene()
    {
        return SceneManager.GetActiveScene().name == "End";
    }

    private bool IsMenuScene()
    {
        return SceneManager.GetActiveScene().name == "Menu";
    }

    private void Update()
    {
        if (IsMenuScene())
        {
            if(playerScore != 0 || npcScore != 0)
            {
                playerScore = 0;
                npcScore = 0;
            }            
        }
        if (IsEndScene())
        {
            if (winner == null)
            {
                winner = GameObject.Find("Winner").GetComponent<Text>();
                winner.text = playerScore == 3 ? "Jugador" : "Enemigo";
            }
            StartCoroutine(RestartGame(3f));
        }
        if (IsGameScene())
        {
            #region RUNTIME REFERENCES
            if(clickSfx == null)
            {
                clickSfx = GameObject.Find("ClickSFX").GetComponent<AudioSource>();
            }
            if(storyScroll == null)
            {
                storyScroll = GameObject.Find("StoryScrollbar").GetComponent<Scrollbar>();
            }
            if (actionsScroll == null)
            {
                actionsScroll = GameObject.Find("ActionsScrollbar").GetComponent<Scrollbar>();
            }
            if (playerScoreText == null)
            {
                playerScoreText = GameObject.Find("PlayerScoreText").GetComponent<Text>();
            }
            if (npcScoreText == null)
            {
                npcScoreText = GameObject.Find("EnemyScoreText").GetComponent<Text>();
            }
            if (btnPrefab == null)
            {
                btnPrefab = (GameObject)Resources.Load("Prefabs/Button", typeof(GameObject));
            }
            if (grid == null)
            {
                grid = GameObject.Find("Grid").GetComponent<RectTransform>();
            }
            if (battleText == null)
            {
                battleText = GameObject.Find("StoryText").GetComponent<Text>();
                battleText.text = "";
            }
            #endregion

            switch (turn)
            {
                case Fighters.NPC:
                    switch (cpuAction)
                    {
                        case NpcActions.INSULT:
                            GetNpcInsult();
                            GetAllResponses();
                            ChangeTurn(Fighters.PLAYER);
                            break;
                        case NpcActions.RESPOND:
                            GetNpcResponse();
                            GetAllInsults();
                            ChangeTurn(Fighters.PLAYER);
                            break;
                    }
                    break;
                case Fighters.PLAYER:
                    break;
            }
        }        
    }
    private IEnumerator RestartGame(float waitTime)
    {
        Destroy(gameObject, waitTime + 0.01f);
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            GetComponent<GameManager>().ReturnToMenu();
        }
    }

    /// <summary>
    /// Al principio del juego o en el caso de responder correctamente al insulto del Player,
    /// el NPC insulta.
    /// </summary>
    public void GetNpcInsult()
    {
        CheckNpcHasInsults();
        int random = Random.Range(0, npcController.AllInsults.Count);
        currentInsult = npcController.AllInsults[random];
        battleText.text += battleText.text == string.Empty ? currentInsult : "\n\n" + currentInsult;
        storyScroll.value = 0;
        //CheckCorrectResponse(NpcActions.RESPOND, Fighters.PLAYER);

        //Evitar que el NPC repita insultos si aún le quedan en la lista
        npcController.AllInsults.RemoveAt(random);
    }

    /// <summary>
    /// Rellenar la lista de insultos si el NPC se ha quedado a 0.
    /// </summary>
    private void CheckNpcHasInsults()
    {
        if (npcController.AllInsults.Count == 0)
        {
            FillNpcInsultList();
        }
    }

    /// <summary>
    /// Separar las preguntas de la lista de acciones para el NPC.
    /// </summary>
    public void FillNpcInsultList()
    {
        battleFiller = new BattleFiller();
        for (int i = 0; i < battleFiller.allBattles.Count; i++)
        {
            npcController.AllInsults.Add(battleFiller.allBattles[i].Insult);
        }
    }

    /// <summary>
    /// Si al Player ha insultado previamente el NPC responde.
    /// </summary>
    public void GetNpcResponse()
    {
        int random = Random.Range(0, battleFiller.allBattles.Count);
        currentResponse = battleFiller.allBattles[random].Response;
        battleText.text += "\n\n" + currentResponse;
        storyScroll.value = 0;

        Battle tempBattle;
        for (int i = 0; i < battleFiller.allBattles.Count; i++)
        {
            if (battleFiller.allBattles[i].Insult == currentInsult)
            {
                tempBattle = battleFiller.allBattles[i];
                if (currentResponse == tempBattle.Response)
                {
                    IncreaseNpcScore();
                    ChangeNpcAction(NpcActions.INSULT);
                    ClearGrid();
                    GetAllResponses();
                }
                else
                {
                    IncreasePlayerScore();
                    ChangeTurn(Fighters.PLAYER);
                }
            }
        }
    }

    private void IncreaseNpcScore()
    {
        if (npcScore < 2)
        {
            npcScore++;
            npcScoreText.text = npcScore.ToString();
        }
        else
        {
            npcScore++;
            GetComponent<GameManager>().WinnerScene();
        }
        ReducePlayerScore();
    }

    private void IncreasePlayerScore()
    {
        if (playerScore < 2)
        {
            playerScore++;
            playerScoreText.text = playerScore.ToString();
        }
        else
        {
            playerScore++;
            GetComponent<GameManager>().WinnerScene();
        }
        ReduceNpcScore();
    }

    /// <summary>
    /// Restar puntuación al Player.
    /// </summary>
    /// <param name="score"></param>
    private void ReducePlayerScore()
    {
        if (playerScore > 0)
        {
            playerScore--;
            playerScoreText.text = playerScore.ToString();
        }
    }

    /// <summary>
    /// Restar puntuación al NPC.
    /// </summary>
    /// <param name="score"></param>
    private void ReduceNpcScore()
    {
        if (npcScore > 0)
        {
            npcScore--;
            npcScoreText.text = npcScore.ToString();
        }
    }

    /// <summary>
    /// Mostrar en pantalla todos los botones con insultos.
    /// </summary>
    public void GetAllInsults()
    {
        ClearGrid();
        for (int i = 0; i < battleFiller.allBattles.Count; i++)
        {
            GameObject tempBtn = Instantiate(btnPrefab, grid);
            tempBtn.GetComponentInChildren<Text>().text = battleFiller.allBattles[i].Insult;
            FillListener(tempBtn, i);
        }        
    }

    /// <summary>
    /// Mostrar en pantalla todos los botones con respuestas.
    /// </summary>
    public void GetAllResponses()
    {
        ClearGrid();
        for (int i = 0; i < battleFiller.allBattles.Count; i++)
        {
            GameObject tempBtn = Instantiate(btnPrefab, grid);
            tempBtn.GetComponentInChildren<Text>().text = battleFiller.allBattles[i].Response;
            FillListener(tempBtn, i);            
        }
    }

    /// <summary>
    /// Asignar al botón la acción que va a realizar.
    /// </summary>
    /// <param name="button">Prefab del botón</param>
    /// <param name="input">Índice de la acción en la lista de batalla</param>
    private void FillListener(GameObject button, int input)
    {
        switch (cpuAction)
        {
            case NpcActions.INSULT:
                button.GetComponent<Button>().onClick.AddListener(() => 
                { 
                    SetResponseButton(input);
                    PlaySfx(clickSfx);
                });
                break;
            case NpcActions.RESPOND:
                button.GetComponent<Button>().onClick.AddListener(() => 
                {
                    SetInsultButton(input);
                    PlaySfx(clickSfx);
                });
                break;
        }        
    }

    private void PlaySfx(AudioSource sfx)
    {
        sfx.Play();
    }

    /// <summary>
    /// Eliminar todos los botones del "grid" por si tienen que intanciarse otros diferentes.
    /// </summary>
    private void ClearGrid()
    {
        foreach (Button child in grid.GetComponentsInChildren<Button>())
        {
            Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// Permitir al Player insultar pulsando el botón.
    /// </summary>
    /// <param name="index">Índice de la acción en la lista de batalla</param>
    private void SetInsultButton(int index)
    {
        currentInsult = battleFiller.allBattles[index].Insult;
        battleText.text += "\n\n" + currentInsult;
        ChangeTurn(Fighters.NPC);
    }

    /// <summary>
    /// Permitir que el Player responda a un insulto de el NPC.
    /// </summary>
    /// <param name="index">Índice de la acción en la lista de batalla</param>
    private void SetResponseButton(int index)
    {
        currentResponse = battleFiller.allBattles[index].Response;
        battleText.text += "\n\n" + currentResponse;
        Battle tempBattle;
        for (int i = 0; i < battleFiller.allBattles.Count; i++)
        {
            if(battleFiller.allBattles[i].Insult == currentInsult)
            {
                tempBattle = battleFiller.allBattles[i];
                if (currentResponse == tempBattle.Response)
                {

                    IncreasePlayerScore();
                    ChangeNpcAction(NpcActions.RESPOND);
                    GetAllInsults();
                }
                else
                {
                    IncreaseNpcScore();
                    ChangeTurn(Fighters.NPC);
                }
            }
        }
    }

    /// <summary>
    /// Cambiar el turno del jugador.
    /// </summary>
    /// <param name="figter"></param>
    private void ChangeTurn(Fighters figter)
    {
        turn = figter;
        Debug.Log(turn);
    }

    /// <summary>
    /// Cambiar la acción de el NPC en el combate según la acción realizada por el Player.
    /// </summary>
    /// <param name="action"></param>
    private void ChangeNpcAction(NpcActions action)
    {
        cpuAction = action;
        Debug.Log("NPC " + cpuAction);
    }
}

